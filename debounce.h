
#include "timedEvent.h"

// this class debounces an input value by using a state machine and a few timers.
//
typedef enum DebounceState
{
    DebounceIdle,
    DebouncePressing,
    DebouncePressed,
    DebounceHolding,
    DebounceReleasing
} DebounceStateType;

class Debouncer
{
   private:
     int lastValue;
     DebounceStateType state;
     timedEvent timing;
     int pressDelay;
     int holdDelay = 1000;
     int repeatDelay = 200;
    
   public:
     Debouncer(int pressDelay)
     {
       this->pressDelay = pressDelay;
     };
     Debouncer()
     {
       this->pressDelay  = 100;
     };
     
     void setPressDelay(int delay)
     {
       this->pressDelay = delay;
     }
     
     int debounce(int value)
     {
        switch(state) 
        {
          case DebounceIdle:
            if(value != lastValue)
            {
              state = DebouncePressing;
              lastValue = value;
              timing.reset();
            }
            break;
          case DebouncePressing:
            if(value != lastValue)
            {
               state = DebounceIdle; 
            }
            else
            {
               if(timing.hasElapsed(pressDelay))
               {
                  state = DebouncePressed; 
                  timing.reset();
                  return value;
               }
            }
            break;
          case DebouncePressed:
            if(value != lastValue)
            {
                state = DebounceIdle;
            }
            else
            {
                //key held, do a timer to start repeating values.
               if(timing.hasElapsed(holdDelay))
               {
                  state = DebounceHolding; 
                  return value;
               }
            }
            break;
          case DebounceHolding:
            if(value != lastValue)
            {
                state = DebounceIdle;
            }
            else
            {
                //key held, do a timer to start repeating values.
               if(timing.hasElapsed(repeatDelay))
               {                 
                  return value;
               }
            }
            break;
          case DebounceReleasing:
            if(value == lastValue)
            {
               state = DebouncePressed;
            }
            else
            {
              if(timing.hasElapsed(100u)) 
               {
                  state = DebounceIdle; 
               }
            }
            
            break;
        }
        
        return 0;
     };
};



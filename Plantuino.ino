#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <EEPROM.h>

/* power saving includes */
#include <avr/interrupt.h>
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/io.h>

#include "debounce.h"

const uint8_t PIN_MOISTURE_SENSOR = A0;
const uint8_t PIN_BUTTON_SELECT = 2;
const uint8_t PIN_BUTTON_UP = 3;
const uint8_t PIN_BUTTON_DOWN = 4;
const uint8_t PIN_PUMP = 5;          //UNO pwm pin on 3,5,6,9,10
const uint8_t PIN_LCD_LIGHT = 6;

const uint8_t PIN_LCD_RST = 8;
const uint8_t PIN_LCD_DC = 9;
const uint8_t PIN_LCD_CS = 10;

int minimumMoistureLevel = 500;
const int moistureBandwidth = 50; 
const int stepsPerAdjustment = 25;

Debouncer keyUpDebounce = Debouncer(50);
Debouncer keyDownDebounce = Debouncer(50);
Debouncer keySelectDebounce = Debouncer(50);
timedEvent readSensorEvent;
timedEvent screenSaver;
timedEvent batteryEvent;

long lastValue;
long average;

int lowestValue = 9999;
int highestValue = 0;

int lastVoltage;

bool pumpOn = false;
bool updateDisplay = false;

volatile bool watchdogActivated = true;

Adafruit_PCD8544 display = Adafruit_PCD8544(PIN_LCD_DC, PIN_LCD_CS, PIN_LCD_RST);

void setup() {
  
    // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial);
  
  Serial.println("Plantuino 1.2\nRob van der Veer"); 
  
  //pinmodes
  pinMode(PIN_BUTTON_UP, INPUT_PULLUP);
  pinMode(PIN_BUTTON_DOWN, INPUT_PULLUP);
  pinMode(PIN_BUTTON_SELECT, INPUT_PULLUP);
  pinMode(PIN_PUMP, OUTPUT);
  pinMode(PIN_MOISTURE_SENSOR, INPUT);
  pinMode(PIN_LCD_LIGHT, OUTPUT);
  
  display.begin();
  display.setContrast(0x3f);
  display.setRotation(2);
  display.clearDisplay();

  // load the setting from EEPROM
  if(EEPROM.read(0) == 0x18)  //valid settings.
  {
    Serial.println("Loading settings:");
    minimumMoistureLevel = EEPROMReadInt(1);
    Serial.print("level = ");
    Serial.println(minimumMoistureLevel);
  }
  else
  {
    Serial.println("no settings, using defaults");
  }
  
  setupSleep();
  
  Serial.println("boot ready");
  
  //prime the average with a fresh reading.
  average = analogRead(PIN_MOISTURE_SENSOR);
  
  enableBacklight();
  
  display.clearDisplay();
  display.setTextColor(BLACK);
  display.setTextSize(1);
  display.setCursor(0,0);
  display.println("PLANTUINO");
  display.setTextSize(1);
  display.println("v1.2 2015");
  display.println("robv.nl");
  display.display();
  
  delay(2000);
  
  enableBacklight();
  
  lastVoltage = readVcc();
}

void loop() 
{
  if(screenSaver.hasElapsed(5000, false))
  {
    //put the display to sleep after 5 seconds.
    
    //sleep_on
    display.command(PCD8544_FUNCTIONSET); //V=0; H=0;
    display.command(0x08);
    display.command(0x24);

    digitalWrite(PIN_LCD_LIGHT, HIGH);  
    
    //if we are not pumping, sleep the cpu.
    if(!pumpOn)
    {
      sleep();
    }
  }
  
  if(batteryEvent.hasElapsed(1000))
  {
     lastVoltage = readVcc();
  }

  if(readSensorEvent.hasElapsed(100) || watchdogActivated)
  {  
    long weight = 200;
    long newValue = analogRead(PIN_MOISTURE_SENSOR);
    
    //correct the newValue to be relative to lastVoltage;
    //newValue = (newValue * lastVoltage) / 5000;

    average = ((newValue * weight) + average * (256-weight))/256;
    if(average != lastValue)
    {
      lastValue = average;
      updateDisplay = true;
      
      Serial.println(average);
    }
    
    //low value = WET
    //high value = DRY
    if(lastValue < lowestValue)
    {
      lowestValue = lastValue;
      updateDisplay = true;
    }
    
    if(lastValue > highestValue)
    {
      highestValue = lastValue;
      updateDisplay = true;
    }
   
   if(average < minimumMoistureLevel && pumpOn == false)
    {
      //turn the pump on, it is getting dry here.
      digitalWrite(PIN_PUMP, HIGH);
      pumpOn = true;
      updateDisplay = true; 
      
      Serial.println("pump on");
      enableBacklight();
    }
    
     if(average > minimumMoistureLevel + moistureBandwidth && pumpOn == true)
    {
      //turn off the pump, we are conducting like crazy here.
      digitalWrite(PIN_PUMP, LOW);
      pumpOn = false;
      updateDisplay = true;
      
      Serial.println("pump off");
      enableBacklight();
    } 
  

    watchdogActivated = false;
  }
  
  bool up = digitalRead(PIN_BUTTON_UP) == false;
  bool down = digitalRead(PIN_BUTTON_DOWN) == false;
  bool select = digitalRead(PIN_BUTTON_SELECT) == false;
  
  if(keyUpDebounce.debounce(up))
  {
     enableBacklight();
     
     if(minimumMoistureLevel < 1000)
     {
       minimumMoistureLevel += stepsPerAdjustment;
     }
     else
     {
       minimumMoistureLevel = 1000;
     }
     
     saveSettings();
  }
  
  if(keyDownDebounce.debounce(down))
  {
     enableBacklight();
     
     if(minimumMoistureLevel > 100)
     {
       minimumMoistureLevel -= stepsPerAdjustment;
     }
     else
     {
       minimumMoistureLevel = 100;
     }
     
     saveSettings();
  }
  
  if(keySelectDebounce.debounce(select))
  {
      enableBacklight();
  }

  if(updateDisplay)
  {
      display.clearDisplay();
      display.setTextColor(BLACK);
      display.setTextSize(2);
      display.setCursor(0,0);
      display.println(average);
      display.setTextSize(1);
      display.print("Set: ");
      display.print(minimumMoistureLevel);
      display.print("-");
      display.println(minimumMoistureLevel + moistureBandwidth);
      
      display.print("Min:");
      display.println(lowestValue);
      display.print("Max:");
      display.println(highestValue);
 
      if(pumpOn)
      {
        display.print("PUMP ");
      }
     
      display.println(lastVoltage);
     
      display.display();
      updateDisplay = false;
  }
 
  
  delay(10);
}

/****** HELPER METHODS *******/

void enableBacklight()
{
  digitalWrite(PIN_LCD_LIGHT, LOW);
  screenSaver.reset();
  
  //sleep off
  display.command(PCD8544_FUNCTIONSET); //V=0; H=0;
  display.command(0x0c);
  
  updateDisplay = true;
}

void saveSettings()
{
  EEPROMWriteInt(1, minimumMoistureLevel);
  
  //mark all settings valid.
  EEPROM.write(0, 0x18);
}


//helper functions
void EEPROMWriteInt(int p_address, unsigned int p_value)
{
     byte lowByte = ((p_value >> 0) & 0xFF);
     byte highByte = ((p_value >> 8) & 0xFF);

     EEPROM.write(p_address, lowByte);
     EEPROM.write(p_address + 1, highByte); 
}

unsigned int EEPROMReadInt(int p_address)
{
     byte lowByte = EEPROM.read(p_address);
     byte highByte = EEPROM.read(p_address + 1);

     return ((lowByte << 0) & 0xFF) + ((highByte << 8) & 0xFF00);
}

/****** SLEEP METHODS *******/

void setupSleep()
{
  noInterrupts();
    
  // Set the watchdog reset bit in the MCU status register to 0.
  MCUSR &= ~(1<<WDRF);
  
  // Set WDCE and WDE bits in the watchdog control register.
  WDTCSR |= (1<<WDCE) | (1<<WDE);

  // Set watchdog clock prescaler bits to a value of 8 seconds.
  WDTCSR = (1<<WDP0) | (1<<WDP3);
  
  // Enable watchdog as interrupt only (no reset).
  WDTCSR |= (1<<WDIE);
  
  interrupts();
}


// Define watchdog timer interrupt.
ISR(WDT_vect)
{
  // Set the watchdog activated flag.
  // Note that you shouldn't do much work inside an interrupt handler.
  watchdogActivated = true;
}

void pin2Interrupt(void)
{
  detachInterrupt(0);
  
  screenSaver.reset();
  readSensorEvent.reset();
}

// Put the Arduino to sleep.
void sleep()
{   
  // Set sleep to full power down.  Only external interrupts or 
  // the watchdog timer can wake the CPU!
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  
  attachInterrupt(0, pin2Interrupt, LOW);
  
  // Turn off the ADC while asleep.
  //power_adc_disable();

  // Enable sleep and enter sleep mode.
  sleep_mode();

  // CPU is now asleep and program execution completely halts!
  // Once awake, execution will resume at this point.
  
  // When awake, disable sleep mode and turn on all devices.
  sleep_disable();
  power_all_enable();
}

/* readVcc */

int readVcc() 
{
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif  
 
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring
 
  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
  uint8_t high = ADCH; // unlocks both
 
  long result = (high<<8) | low;
 
  //result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  result = 1061603L / result;
  return result; // Vcc in millivolts
}
